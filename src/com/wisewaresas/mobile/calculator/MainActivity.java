package com.wisewaresas.mobile.calculator;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity implements OnClickListener {

	private TextView display;
	private TextView operationDisplay;

	private Button buttonCero;
	private Button buttonOne;
	private Button buttonTwo;
	private Button buttonThree;
	private Button buttonFour;
	private Button buttonFive;
	private Button buttonSix;
	private Button buttonSeven;
	private Button buttonEight;
	private Button buttonNine;
	private Button buttonClear;
	private Button buttonPlus;
	private Button buttonMinus;
	private Button buttonMultiplication;
	private Button buttonDivide;
	private Button buttonPoint;
	private Button buttonSen;
	private Button buttonCos;
	private Button buttonTan;
	private Button buttonEqual;
	private Button buttonSquareRoot;

	private double operatorA, operatorB, result;
	private int operation;
	private boolean secondOperatorInput;
	private boolean alreadyTypedSecondOperator;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		this.display = (TextView) findViewById(R.id.calculator_display);
		this.operationDisplay = (TextView) findViewById(R.id.operation_display);
		this.buttonCero = (Button) findViewById(R.id.button_cero);
		this.buttonCero.setOnClickListener(this);
		this.buttonOne = (Button) findViewById(R.id.button_one);
		this.buttonOne.setOnClickListener(this);
		this.buttonTwo = (Button) findViewById(R.id.button_two);
		this.buttonTwo.setOnClickListener(this);
		this.buttonThree = (Button) findViewById(R.id.button_three);
		this.buttonThree.setOnClickListener(this);
		this.buttonFour = (Button) findViewById(R.id.button_four);
		this.buttonFour.setOnClickListener(this);
		this.buttonFive = (Button) findViewById(R.id.button_five);
		this.buttonFive.setOnClickListener(this);
		this.buttonSix = (Button) findViewById(R.id.button_six);
		this.buttonSix.setOnClickListener(this);
		this.buttonSeven = (Button) findViewById(R.id.button_seven);
		this.buttonSeven.setOnClickListener(this);
		this.buttonEight = (Button) findViewById(R.id.button_eight);
		this.buttonEight.setOnClickListener(this);
		this.buttonNine = (Button) findViewById(R.id.button_nine);
		this.buttonNine.setOnClickListener(this);
		this.buttonClear = (Button) findViewById(R.id.button_clear);
		this.buttonClear.setOnClickListener(this);
		this.buttonPlus = (Button) findViewById(R.id.button_plus);
		this.buttonPlus.setOnClickListener(this);
		this.buttonMinus = (Button) findViewById(R.id.button_minus);
		this.buttonMinus.setOnClickListener(this);
		this.buttonMultiplication = (Button) findViewById(R.id.button_multiplication);
		this.buttonMultiplication.setOnClickListener(this);
		this.buttonDivide = (Button) findViewById(R.id.button_divide);
		this.buttonDivide.setOnClickListener(this);
		this.buttonSen = (Button) findViewById(R.id.button_sen);
		this.buttonSen.setOnClickListener(this);
		this.buttonCos = (Button) findViewById(R.id.button_cos);
		this.buttonCos.setOnClickListener(this);
		this.buttonTan = (Button) findViewById(R.id.button_tan);
		this.buttonTan.setOnClickListener(this);
		this.buttonPoint = (Button) findViewById(R.id.button_point);
		this.buttonPoint.setOnClickListener(this);
		this.buttonEqual = (Button) findViewById(R.id.button_equal);
		this.buttonEqual.setOnClickListener(this);
		this.buttonSquareRoot = (Button) findViewById(R.id.button_square_root);
		this.buttonSquareRoot.setOnClickListener(this);

		this.secondOperatorInput = false;
		this.alreadyTypedSecondOperator = false;
		this.operatorA = 0;
		this.operatorB = 0;
		this.result = 0;
		this.operation = 0;
		this.display.setText("0");

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.button_cero:
			writeNumberInDisplay("0");
			break;

		case R.id.button_one:
			writeNumberInDisplay("1");
			break;

		case R.id.button_two:
			writeNumberInDisplay("2");
			break;

		case R.id.button_three:
			writeNumberInDisplay("3");
			break;

		case R.id.button_four:
			writeNumberInDisplay("4");
			break;

		case R.id.button_five:
			writeNumberInDisplay("5");
			break;

		case R.id.button_six:
			writeNumberInDisplay("6");
			break;

		case R.id.button_seven:
			writeNumberInDisplay("7");
			break;

		case R.id.button_eight:
			writeNumberInDisplay("8");
			break;

		case R.id.button_nine:
			writeNumberInDisplay("9");
			break;

		case R.id.button_point:
			writePointInDisplay();
			break;

		case R.id.button_clear:
			this.operatorA = 0;
			this.operatorB = 0;
			this.result = 0;
			this.operation = 0;
			this.display.setText("0");
			this.operationDisplay.setText("");
			break;

		case R.id.button_equal:
			operationResult(false);
			break;

		case R.id.button_sen:
			operationResultWithOneOperator(1);
			break;

		case R.id.button_cos:
			operationResultWithOneOperator(2);
			break;

		case R.id.button_tan:
			operationResultWithOneOperator(3);
			break;

		case R.id.button_plus:
			saveOperationWithTwoOperators(1);
			this.operationDisplay.setText(this.buttonPlus.getText());
			break;

		case R.id.button_minus:
			saveOperationWithTwoOperators(2);
			this.operationDisplay.setText(this.buttonMinus.getText());
			break;

		case R.id.button_multiplication:
			saveOperationWithTwoOperators(3);
			this.operationDisplay.setText(this.buttonMultiplication.getText());
			break;

		case R.id.button_divide:
			saveOperationWithTwoOperators(4);
			this.operationDisplay.setText(this.buttonDivide.getText());
			break;

		case R.id.button_square_root:
			operationResultWithOneOperator(4);
			break;

		default:
			break;
		}
	}

	public void writeNumberInDisplay(String number) {
		try {
			String currentDisplay = this.display.getText().toString();
			String nextDisplay = "";
			if (this.secondOperatorInput) {
				this.secondOperatorInput = false;
				currentDisplay = "";
				this.alreadyTypedSecondOperator = true;
			}
			if (currentDisplay.equalsIgnoreCase("") == true
					|| Double.parseDouble(currentDisplay) == 0) {
				if (currentDisplay.equalsIgnoreCase("0.") == true) {
					nextDisplay = currentDisplay + number;
				} else {
					nextDisplay = number;
				}
			} else {
				nextDisplay = currentDisplay + number;
			}
			this.display.setText(nextDisplay.toString());
		} catch (Exception e) {
			this.operatorA = 0;
			this.operatorB = 0;
			this.result = 0;
			this.operation = 0;
			this.display.setText("0");
			this.operationDisplay.setText("");
		}
	}

	public void writePointInDisplay() {
		try {
			String currentDisplay = this.display.getText().toString();
			String nextDisplay = "";
			if (this.secondOperatorInput) {
				this.secondOperatorInput = false;
				currentDisplay = "";
				this.alreadyTypedSecondOperator = true;
			}
			if (currentDisplay.equalsIgnoreCase("") == true) {
				nextDisplay = "0.";
			} else {
				try {
					int proofInt = Integer.parseInt(currentDisplay);
					nextDisplay = currentDisplay + ".";
				} catch (Exception e) {
					nextDisplay = currentDisplay;
				}
			}
			this.display.setText(nextDisplay.toString());
		} catch (Exception e) {
			this.operatorA = 0;
			this.operatorB = 0;
			this.result = 0;
			this.operation = 0;
			this.display.setText("0");
			this.operationDisplay.setText("");
		}
	}

	public void operationResult(boolean operationCeroByOperator) {
		String currentDisplay = this.display.getText().toString();
		String nextDisplay = "";
		boolean error = false;
		if (currentDisplay.equalsIgnoreCase("") == false) {
			try {
				this.secondOperatorInput = false;
				this.operatorB = Double.parseDouble(this.display.getText()
						.toString());
				switch (this.operation) {
				case 1:
					this.result = this.operatorA + this.operatorB;
					break;
				case 2:
					this.result = this.operatorA - this.operatorB;
					break;
				case 3:
					this.result = this.operatorA * this.operatorB;
					break;
				case 4:
					try {
						this.result = this.operatorA / this.operatorB;
					} catch (Exception e) {
						error = true;
					}

					break;
				default:
					break;
				}
				if (this.operation != 0) {
					if (error) {
						this.display.setText("Error");
					} else {
						nextDisplay = String.valueOf(result);
						this.display.setText(nextDisplay);
					}

				}
				if (!operationCeroByOperator) {
					this.operation = 0;
				}				
				this.operationDisplay.setText("");
				this.alreadyTypedSecondOperator = false;

			} catch (Exception e) {
				this.operatorA = 0;
				this.operatorB = 0;
				this.result = 0;
				this.operation = 0;
				this.display.setText("0");
				this.operationDisplay.setText("");
			}
		}
	}

	public void operationResultWithOneOperator(int operationWithOneOperator) {
		String currentDisplay = this.display.getText().toString();
		String nextDisplay = "";
		boolean error = false;
		this.operation = 0;
		if (currentDisplay.equalsIgnoreCase("") == false) {
			try {
				this.secondOperatorInput = false;
				this.operatorB = Double.parseDouble(this.display.getText()
						.toString());
				switch (operationWithOneOperator) {
				case 1:
					this.result = Math.cos(this.operatorB);
					break;
				case 2:
					this.result = Math.sin(this.operatorB);
					break;
				case 3:
					try {
						this.result = Math.tan(this.operatorB);
					} catch (Exception e) {
						error = true;
					}
					break;
				case 4:
					this.result = Math.sqrt(this.operatorB);
					break;
				default:
					break;
				}
				if (error) {
					this.display.setText("Error");
				} else {
					nextDisplay = String.valueOf(result);
					this.display.setText(nextDisplay);
				}
				this.operationDisplay.setText("");				

			} catch (Exception e) {
				this.operatorA = 0;
				this.operatorB = 0;
				this.result = 0;
				this.operation = 0;
				this.display.setText("0");
				this.operationDisplay.setText("");
			}
		}
	}

	public void saveOperationWithTwoOperators(int operation) {
		try {
			if (this.operation != 0 && this.alreadyTypedSecondOperator) {
				operationResult(true);
			}
			this.operation = operation;
			this.secondOperatorInput = true;
			this.operatorA = Double.parseDouble(this.display.getText()
					.toString());
		} catch (Exception e) {
			this.operatorA = 0;
			this.operatorB = 0;
			this.result = 0;
			this.operation = 0;
			this.display.setText("0");
			this.operationDisplay.setText("");
		}
	}

}
